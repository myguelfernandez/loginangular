import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class LoginService {
  url = '/ServiVuelos-web/';

  constructor(private http: HttpClient) {}

  login() {
    const request = {
      user: 'miguel',
      pass: '123',
      uid: 'a9025a67-c0c9-445f-8387-023af105574c',
      terminal: 'terminal',
    };

    return this.http.post(`${this.url}webresources/sesion`, request).pipe(
      map((resp) => {
        localStorage.setItem('loginData', JSON.stringify(resp));
        return resp;
      })
    );
  }
}
