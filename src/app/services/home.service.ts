import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HomeService {
  url = '/ServiVuelos-web/';

  constructor(private http: HttpClient) { }

  noticias(){
    return this.http.get(`${this.url}webresources/noticia/listar`);
  }
}
